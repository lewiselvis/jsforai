// Script zur Überarbeitung von speziellen handgeschribenen PostScript Dateien
// Damit werden Die Ebenen, Farben und Konturen richtig eingstellt.
// (Umwandeln der Spotfarben zu Ebenen mit entsprechendem Namen, Sichtbarkeit e.t.c) 

// #####################
// A: Variablen anlegen:

/* Sortierung von Ebenen gemäß nachfolgender Liste
   (in der Liste sind die Namen, soe wie sie am Ende in der Ebenenpalette erscheinen sollen) */
var list = [
	"Register",
	"Text",
	"ED_strich",
	"oesen",
	"oe_weiss",
	"oe_strich",
	"whiteline",
	"Motiv",
	"bbox",
	"DRT",
	"Schneiden"
];
/* Ebenen die ihre Konturfarbe behalten sollen werden am Ende gesperrt.
   Bei allen anderen wird die Konturfarbe aller Objekte (pathItems) gelöscht (noColor) */
var keepStroke_Ar = [
	"ED_strich",
	"oesen",
	"oe_weiss",
	"oe_strich",
	"whiteline",
	"Motiv"
];	/* muss gleich in String gewandelt werden, 
	   damit die Funktion »indexOf« angewendet werden kann */
var keepStroke = keepStroke_Ar.toString();

/* Ebenen die am Ende leer sind und gelöscht werden sollen */
var layerToDel_Ar = [
	"[Passermarken]",
	"Text2",
	"Ebene 1"
];

var noColor = new NoColor();

// #####################
// B: Programmschleife:

if ( app.documents.length > 0  ) {
	doc = app.activeDocument;
	var NameMitPfad = doc.fullName;
	// alert(NameMitPfad );

	// Ebenen aus Spotfarben anlegen
	var anzSPOTFarben= doc.spots.length;
	for (var i = 0; i< anzSPOTFarben;i++) {
		var aktName = doc.spots[i].name;
		var myNewLayer = doc.layers.add();
		myNewLayer.name = aktName;
	}
	for (var j = 0; j < doc.pathItems.length; j++ ) {
		pathRef = doc.pathItems[j];		
		if ((pathRef.stroked) && (pathRef.strokeColor.typename=="SpotColor")){
			var aktFarbe = pathRef.strokeColor.spot.color;
			var aktSPOTFarbe = pathRef.strokeColor.spot;
			var zielEbenenName = aktSPOTFarbe.name;
			pathRef.move (doc.layers[zielEbenenName], ElementPlacement.PLACEATBEGINNING);
		}
		if ((pathRef.filled) && (pathRef.fillColor.typename=="SpotColor")){
			var aktFarbe	    = pathRef.fillColor.spot.color;
			var aktSPOTFarbe =pathRef.fillColor.spot;
			var zielEbenenName = aktSPOTFarbe.name;
			pathRef.move (doc.layers[zielEbenenName], ElementPlacement.PLACEATBEGINNING);
		}
	}
	texte= doc.textFrames;
	textanzahl=texte.length;
	if ( textanzahl > 0 ) {
		for (var m = 0; m< textanzahl ;m++) {
			texte[0].move (doc.layers["Text"], ElementPlacement.PLACEATBEGINNING);
			texte[0].createOutline();
		}
	}
	unlockAllLayers();
	// alert(listAllLayers());
	removeUnusedLayers(doc, layerToDel_Ar);
	sort_layer (doc, list);
	for (var p = doc.swatches.length-1; p>1;p--) {
	}
	if ( doc.pathItems.length > 0 ) {
		thePaths = doc.pathItems;
		numPaths = thePaths.length;
		for ( i = 0; i < doc.pathItems.length; i++ ) {
			pathArt = doc.pathItems[i];
			if   ( pathArt.parent.locked == false ) {
				pathArt.strokeColor = noColor;
			}
		}
	}	
	unlockAllLayers();
	redraw();
	exportFileAsEPS(NameMitPfad);
}

// #####################
// C: verwendete Funktionen:

function unlockAllLayers () {
	for (var i = 0; i< doc.layers.length;i++) {
		doc.layers[i].locked = false;
	}
}
function listAllLayers () {
	var ebenenNamen = new Array();
	for (var lli = 0; lli< doc.layers.length;lli++) {
		ebenenNamen.push(doc.layers[lli].name+"_ind-"+lli);
	}
	return ebenenNamen.toString();
}
function removeUnusedLayers(obj, del_ar) {
	var anzEbenenToDel=del_ar.length;
	for (var rmi = 0; rmi< anzEbenenToDel;rmi++) {
		try {
			var aktLayer = obj.layers.getByName(del_ar[rmi]);
			aktLayer.remove();
		} catch(e) {}
	}
}
function sort_layer (obj, array) {
	for (var ri=0, riL=array.length; ri < riL ; ri++) {
		try {
			// nicht jede Ebene aus obiger Liste kommt im doc vor!
			var aktLayer = obj.layers.getByName(array[ri]);
			aktLayer.zOrder( ZOrderMethod.SENDTOBACK );
			// zugleich hier Ebenen temporär sperren, welche ihre Konturen behalten sollen:
			if (keepStroke.indexOf(aktLayer.name) >= 0) {
				aktLayer.locked = true;
			}
		} catch(e) {}
	};
}
function exportFileAsEPS (destFile) {
	var newFile = new File(destFile);
	var saveDoc;
	if ( app.documents.length == 0 )
		saveDoc = app.documents.add();
	else
	saveDoc = app.activeDocument;
	var saveOpts = new EPSSaveOptions();
	saveOpts.preview=EPSPreview.None;
	saveOpts.compatibility =Compatibility.ILLUSTRATOR8;
	saveOpts.postScript=EPSPostScriptLevelEnum.LEVEL2;
	saveOpts.embedAllFonts = false;
	saveOpts.includeDocumentThumbnails = false;
	saveOpts.saveMultipleArtboards = false;
	// saveOpts.ArtboardRange ="1";
	saveDoc.saveAs( newFile, saveOpts );
}	



