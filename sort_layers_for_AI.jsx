/**
 * sort layers
found: https://github.com/milligramme

Sortierung von Ebenen gemäß nachfolgender Liste
(in der Liste sind die Namen, soe wie sie am Ende in der Ebenenpalette erscheinen sollen)
 */
var list = new Array();

list.push("Register");
list.push("Text");
list.push("oesen");
list.push("bbox" );
list.push("whiteline" );
list.push("Motiv" );
list.push("DRT" );
list.push("Schneiden" );

var doc = app.documents[0];
sort_layer (doc, list);

function sort_layer (obj, array) {
	for (var ri=0, riL=array.length; ri < riL ; ri++) {
		try {
			// nicht jede Ebene aus obiger Liste kommt im doc vor!
			obj.layers.getByName(array[ri]).zOrder( ZOrderMethod.SENDTOBACK );
		} catch(e) {}
	};
}

