// Sets the stroke and fill of a path item to colors of a randomly selected swatch

// Version 17: EDstrich Ebene wird jetzt auch gesperrt
// zuvor wurde schon das automatische speichern eigefügt als Illu 8
// der Edstrich soll nur noch bei Textil erscheinen - muß in XYLO Template (45) eingestellt werden
if ( app.documents.length > 0  ) {
	doc = app.activeDocument;
	var NameMitPfad = doc.fullName;
	//alert(NameMitPfad );

	// Ebenen aus Spotfarben anlegen
	var anzSPOTFarben= doc.spots.length;
	for (var i = 0; i< anzSPOTFarben;i++) {
			var aktName = doc.spots[i].name
			
			if (aktName == "oesen" ) {
				var oesenfarb = new CMYKColor();
				oesenfarb = doc.spots[i].color;
				var oesenalsSpot = new SpotColor();
				oesenalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;
			}
			if (aktName == "oe_weiss" ) {
				var oe_weissfarb = new CMYKColor();
				oe_weissfarb = doc.spots[i].color;
				var oe_weissalsSpot = new SpotColor();
				oe_weissalsSpot=doc.spots[i];
			}
			if (aktName == "oe_strich" ) {
				var oe_strichfarb = new CMYKColor();
				oe_strichfarb = doc.spots[i].color;
				var oe_strichalsSpot = new SpotColor();
				oe_strichalsSpot=doc.spots[i];
			}
		
			if (aktName == "ED_strich" ) {
				var ED_strichfarb = new CMYKColor();
				ED_strichfarb = doc.spots[i].color;
				var ED_strichalsSpot = new SpotColor();
				ED_strichalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;
			}
			if (aktName == "Schneiden" ) {
				var Schneidenfarb = new CMYKColor();
				Schneidenfarb = doc.spots[i].color;
				var SchneidenalsSpot = new SpotColor();
				SchneidenalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;
			}
			if (aktName == "DRT" ) {
				var DRTfarb = new CMYKColor();
				DRTfarb = doc.spots[i].color;
				var DRTalsSpot = new SpotColor();
				DRTalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;
			}
			if (aktName == "Motiv" ) {
				var Motivfarb = new CMYKColor();
				Motivfarb = doc.spots[i].color;
				var MotivalsSpot = new SpotColor();
				MotivalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;	
			}
			if (aktName == "whiteline" ) {
				var whitelinefarb = new CMYKColor();
				whitelinefarb = doc.spots[i].color;
				var whitelinealsSpot = new SpotColor();
				whitelinealsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;	
				
			}
			if (aktName == "Rillen" ) {
				var Rillenfarb = new CMYKColor();
				Rillenfarb = doc.spots[i].color;
				var RillenalsSpot = new SpotColor();
				RillenalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;
			}
			if (aktName == "bbox" ) {
				var bboxfarb = new CMYKColor();
				bboxfarb = doc.spots[i].color;
				var bboxalsSpot = new SpotColor();
				bboxalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;
			}
			if (aktName == "Text" ) {
				var Textfarb = new CMYKColor();
				Textfarb = doc.spots[i].color;
				var TextalsSpot = new SpotColor();
				TextalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;
			}

			if (aktName == "Zeichnen" ) {
				var Zeichnenfarb = new CMYKColor();
				Zeichnenfarb = doc.spots[i].color;
				var ZeichnenalsSpot = new SpotColor();
				ZeichnenalsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;	
			}

			if (aktName == "Register" ) {
				var Registerfarb = new CMYKColor();
				Registerfarb = doc.spots[i].color;
				var RegisteralsSpot = new SpotColor();
				RegisteralsSpot=doc.spots[i];
				doc.layers.add();
				doc.layers[0].name=aktName;
			}
			
			


// so geht das für alle Spotfarben im Doc, auch die nicht verwendeten
/*
				doc.layers.add();
				doc.layers[0].name=aktName;
*/
	}
	var countr = 0;
	for (var j = 0; j < doc.pathItems.length; j++ ) {
		pathRef = doc.pathItems[j];		
		if ((pathRef.stroked) && (pathRef.strokeColor.typename=="SpotColor")){
				// alert(pathRef.strokeColor.spot.color.yellow);
			var aktFarbe	    = pathRef.strokeColor.spot.color;
			var aktSPOTFarbe =pathRef.strokeColor.spot;
			if ( aktSPOTFarbe == DRTalsSpot){
					//pathRef.stroked = true;
					//pathRef.filled = true;
					pathRef.move (doc.layers["DRT"], ElementPlacement.PLACEATBEGINNING);
					
			}
			if ( aktSPOTFarbe == RegisteralsSpot){
					pathRef.move (doc.layers["Register"], ElementPlacement.PLACEATBEGINNING);
					countr++;
			}
		
			if ( aktSPOTFarbe == oesenalsSpot){
					pathRef.move (doc.layers["oesen"], ElementPlacement.PLACEATEND);
			}
			if ( aktSPOTFarbe == oe_weissalsSpot){
					pathRef.move (doc.layers["oesen"], ElementPlacement.PLACEATEND);
			}
			if ( aktSPOTFarbe == oe_strichalsSpot){
					pathRef.move (doc.layers["oesen"], ElementPlacement.PLACEATEND);
			}
			if ( aktSPOTFarbe == ED_strichalsSpot){
					pathRef.move (doc.layers["ED_strich"], ElementPlacement.PLACEATEND);
			}
		
		
			if ( aktSPOTFarbe == SchneidenalsSpot){
					pathRef.move (doc.layers["Schneiden"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == RillenalsSpot){
					pathRef.move (doc.layers["Rillen"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == ZeichnenalsSpot){
					pathRef.move (doc.layers["Zeichnen"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == bboxalsSpot){
					pathRef.move (doc.layers["bbox"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == TextalsSpot){
					pathRef.move (doc.layers["Text"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == whitelinealsSpot){
					pathRef.move (doc.layers["whiteline"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == MotivalsSpot){
					pathRef.move (doc.layers["Motiv"], ElementPlacement.PLACEATBEGINNING);
			}			
		}
		if ((pathRef.filled) && (pathRef.fillColor.typename=="SpotColor")){
				// alert(pathRef.strokeColor.spot.color.yellow);
			var aktFarbe	    = pathRef.fillColor.spot.color;
			var aktSPOTFarbe =pathRef.fillColor.spot;
			if ( aktSPOTFarbe == DRTalsSpot){
					//pathRef.stroked = true;
					//pathRef.filled = true;
					pathRef.move (doc.layers["DRT"], ElementPlacement.PLACEATBEGINNING);
					
			}
			if ( aktSPOTFarbe == RegisteralsSpot){
					pathRef.move (doc.layers["Register"], ElementPlacement.PLACEATBEGINNING);
					countr++;
			}
			
			if ( aktSPOTFarbe == oesenalsSpot){
					pathRef.move (doc.layers["oesen"], ElementPlacement.PLACEATEND);
			}
			if ( aktSPOTFarbe == oe_weissalsSpot){
					pathRef.move (doc.layers["oesen"], ElementPlacement.PLACEATEND);
			}
			if ( aktSPOTFarbe == oe_strichalsSpot){
					pathRef.move (doc.layers["oesen"], ElementPlacement.PLACEATEND);
			}
			if ( aktSPOTFarbe == ED_strichalsSpot){
					pathRef.move (doc.layers["ED_strich"], ElementPlacement.PLACEATEND);
			}		
		
			if ( aktSPOTFarbe == SchneidenalsSpot){
					pathRef.move (doc.layers["Schneiden"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == RillenalsSpot){
					pathRef.move (doc.layers["Rillen"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == ZeichnenalsSpot){
					pathRef.move (doc.layers["Zeichnen"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == bboxalsSpot){
					pathRef.move (doc.layers["bbox"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == TextalsSpot){		
					pathRef.move (doc.layers["Text"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == whitelinealsSpot){
					pathRef.move (doc.layers["whiteline"], ElementPlacement.PLACEATBEGINNING);
			}
			if ( aktSPOTFarbe == MotivalsSpot){
					pathRef.move (doc.layers["Motiv"], ElementPlacement.PLACEATBEGINNING);
			}			
		}

	}

	texte= doc.textFrames;
	textanzahl=texte.length;
	if ( textanzahl > 0 ) {
			for (var m = 0; m< textanzahl ;m++) {
				texte[0].move (doc.layers["Text"], ElementPlacement.PLACEATBEGINNING);
				texte[0].createOutline();
			}
	}
for (var i = 0; i< doc.layers.length;i++) {
doc.layers[i].locked = false;
if (doc.layers[i].name == "Ebene 1") {
	doc.layers[i].remove();
}
}

for (var i = 0; i< doc.layers.length;i++) {
if (doc.layers[i].name == "Text") {
	doc.layers[i].zOrder(ZOrderMethod.BRINGTOFRONT);
}
}
for (var i = 0; i< doc.layers.length;i++) {
if (doc.layers[i].name == "Register") {
	doc.layers[i].zOrder(ZOrderMethod.BRINGTOFRONT);
}
}

for (var i = 0; i< doc.layers.length;i++) {
if (doc.layers[i].name == "whiteline") {
	doc.layers[i].zOrder(ZOrderMethod.SENDTOBACK);

}
}

for (var i = 0; i< doc.layers.length;i++) {
if (doc.layers[i].name == "Motiv") {
doc.layers[i].zOrder(ZOrderMethod.SENDTOBACK);

}
}
for (var i = 0; i< doc.layers.length;i++) {
if (doc.layers[i].name == "DRT") {
	doc.layers[i].zOrder(ZOrderMethod.SENDTOBACK);
}
}
for (var i = 0; i< doc.layers.length;i++) {
if (doc.layers[i].name == "Schneiden") {
	doc.layers[i].zOrder(ZOrderMethod.SENDTOBACK);
}
}

var ebenenzahl = doc.layers.length;
	if ( ebenenzahl > 0 ) {
			for (var m = 0; m< ebenenzahl ;m++) {
				if (doc.layers[m].name == "oesen") {
					doc.layers["oesen"].locked = true;
				}
				if (doc.layers[m].name == "ED_strich") {
					doc.layers["ED_strich"].locked = true;
				}
				if (doc.layers[m].name == "whiteline") {
					doc.layers["whiteline"].locked = true;
				}
				if (doc.layers[m].name == "Motiv") {
					doc.layers["Motiv"].locked = true;
				}
			}
	}


for (var p = doc.swatches.length-1; p>1;p--) {
	// alert("Anzahl swatsches: "+doc.swatches.length+" jetzt nr:"+p)
	doc.swatches[p].remove();
}

alert( countr + "Register Punkte");

}

var noColor = new NoColor();

	if ( doc.pathItems.length > 0 ) {
		thePaths = doc.pathItems;
		numPaths = thePaths.length;
		for ( i = 0; i < doc.pathItems.length; i++ ) {
			pathArt = doc.pathItems[i];
			if   ( pathArt.parent.locked == false ) {
				//pathArt.selected = true;
				pathArt.strokeColor = noColor;

			}
		}
	}
//doc.selection.strokeColor = noColor;

redraw();

function exportFileAsEPS (destFile) {
var newFile = new File(destFile);
var saveDoc;
if ( app.documents.length == 0 )
saveDoc = app.documents.add();
else
saveDoc = app.activeDocument;
var saveOpts = new EPSSaveOptions();
saveOpts.preview=EPSPreview.None;
saveOpts.compatibility =Compatibility.ILLUSTRATOR8;
saveOpts.postScript=EPSPostScriptLevelEnum.LEVEL2;
saveOpts.embedAllFonts = false;
saveOpts.includeDocumentThumbnails = false;

saveDoc.saveAs( newFile, saveOpts );
}	
exportFileAsEPS(NameMitPfad);



